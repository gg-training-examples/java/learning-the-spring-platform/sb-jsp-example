package com.groundgurus.javatraining.sbjsfexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AppController {
  @GetMapping({"/", "/index"})
  public String index(Model model, @RequestParam(defaultValue = "World", required = false) String name) {
    model.addAttribute("name", name);
    return "index";
  }
}
