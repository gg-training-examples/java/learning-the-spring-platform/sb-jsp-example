<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<jsp:include page="fragments/header.jsp"></jsp:include>

<main role="main">
  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Hello, ${name}!</h1>
      <p>This is a sample JSP page running on top of Spring Boot 2.x</p>
      <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
  </div>
</main>

<jsp:include page="fragments/footer.jsp"></jsp:include>
